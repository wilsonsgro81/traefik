# Traefik v1.7.16

## Start

```bash
docker network create web
docker compose up -d
```

## Config traefik

```toml
# traefik.toml
debug = true
logLevel = "DEBUG"
defaultEntryPoints = ["http"]

# Default:
[entryPoints]
  [entryPoints.http]
  address = ":80"

[file]
watch = true

################################################################
# API and dashboard configuration
################################################################

[api]
  address = ":8080"
  insecure = true
  dashboard = true
  debug = true

################################################################
# Docker configuration backend
################################################################


[docker]
  domain = "docker.localhost"
  endpoint = "unix:///var/run/docker.sock"
  xposedbydefault = false
  watch = true


[backends]
  [backends.backendNginx]
    [backends.backendNginx.loadbalancer.stickiness]
    [backends.backendNginx.servers.server1]
    url = "http://nginx:80"


[frontends]
  [frontends.frontend1]
  backend = "backendNginx"
    [frontends.frontend1.routes.test_1]
    rule = "Host:nginx.docker.localhost"
  entrypoints = ["http"]
  passHostHeader = true
  priority = 10

```

## Config docker-compose

```yml
version: '3'
networks:
  web:
    external: true
services:
  proxy:
    image: traefik:v1.7.16
    command: --api --docker --LOGLEVEL=debug
    ports:
      - "80:80"
      - "8080:8080"
    networks:
      - web  
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ./config/traefik.toml:/etc/traefik/traefik.toml
  nginx:
    image: nginx:alpine
    restart: always
    volumes:
      - ./website:/usr/share/nginx/html
      - ./config/default.conf:/etc/nginx/conf.d/default.conf
    networks:
      - web

```

## Config nginx

```conf
server {
    listen       80;
    server_name  nginx.docker.localhost;

    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # proxy the PHP scripts to Apache listening on 127.0.0.1:80
    #
    #location ~ \.php$ {
    #    proxy_pass   http://127.0.0.1;
    #}

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    #location ~ \.php$ {
    #    root           html;
    #    fastcgi_pass   127.0.0.1:9000;
    #    fastcgi_index  index.php;
    #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
    #    include        fastcgi_params;
    #}

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    #location ~ /\.ht {
    #    deny  all;
    #}
}

server {
    listen       80;
    server_name  localhost;

    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # proxy the PHP scripts to Apache listening on 127.0.0.1:80
    #
    #location ~ \.php$ {
    #    proxy_pass   http://127.0.0.1;
    #}

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    #location ~ \.php$ {
    #    root           html;
    #    fastcgi_pass   127.0.0.1:9000;
    #    fastcgi_index  index.php;
    #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
    #    include        fastcgi_params;
    #}

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    #location ~ /\.ht {
    #    deny  all;
    #}
}
```

## url

>[web](http://http://nginx.docker.localhost/)

>[dashboard](http://docker.localhost:8080/dashboard/)
